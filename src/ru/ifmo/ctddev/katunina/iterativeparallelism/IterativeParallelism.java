package ru.ifmo.ctddev.katunina.iterativeparallelism;

import info.kgeorgiy.java.advanced.concurrent.ScalarIP;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

/**
 * It must be used to provide parallel execution.
 **/
public class IterativeParallelism implements ScalarIP {

    /**
     * Entry point.
     * @param args command line arguments
     */
    public static void main(String[] args) {
        System.out.println(Runtime.getRuntime().availableProcessors());
    }

    /**
     * Checks all- or any-predicate on list' elements.
     * @param <T> This describes my type parameter
     * @param threads a number of threads
     * @param all true if all-predicate was given, false if any-
     * @param list a list to check
     * @param predicate a predicate to check on
     * @return matching result
     * @throws InterruptedException if thread was interrupted
     */
    public <T> boolean matches(int threads, boolean all, List<T> list, Predicate<? super T> predicate) throws InterruptedException {
        List<AllMatchWorker<T>> workers = new ArrayList<>();
        int sublistSize = list.size() / threads;
        final Object coordinator = new Object();
        for (int i = 0; i < threads; i++) {
            int finishIndex = i != threads - 1 ? (i+1)*sublistSize : list.size();
            int startIndex = i * sublistSize;
            workers.add(all ? new AllMatchWorker<>(list,startIndex,finishIndex, predicate)
                    : new AnyMatchWorker<>(list, startIndex, finishIndex, predicate));
        }
        List<Thread> workingThreads = new ArrayList<>();
        for (int i = 0; i < workers.size(); i++) {
            workingThreads.add(new Thread(workers.get(i)));
            workingThreads.get(i).start();
        }
        for  (int i = 0; i < workers.size(); i++) {

            workingThreads.get(i).join();
        }
        return workers.stream().anyMatch(w -> !w.getResult()) == (workers.get(0) instanceof AnyMatchWorker);
    }

    /**
     * Finds first min or max on list' elements.
     * @param <T> This describes my type parameter
     * @param threads a number of threads
     * @param max true if max, false if min
     * @param list a list to check
     * @param comparator shows how to compare list' elements
     * @return max or min result
     * @throws InterruptedException if thread was interrupted
     */
    public <T> T choose(int threads, boolean max, List<T> list, Comparator<? super T> comparator) throws InterruptedException {
        List<MinimumWorker<T>> workers = new ArrayList<>();
        int sublistSize = list.size() / threads;
      //  final Object coordinator = new Object();
        for (int i = 0; i < threads; i++) {
            int finishIndex = i != threads - 1 ? (i+1)*sublistSize : list.size();
            int startIndex = i * sublistSize;
            workers.add(max ? new MaximumWorker<>(list,startIndex,finishIndex,
                    comparator)
                    : new MinimumWorker<>(list,startIndex,finishIndex, comparator));
        }
        List<Thread> workingThreads = new ArrayList<>();
        for (int i = 0; i < workers.size(); i++) {
            workingThreads.add(new Thread(workers.get(i)));
            workingThreads.get(i).start();
        }
        for  (int i = 0; i < workers.size(); i++) {

            workingThreads.get(i).join();
        }
        T result = workers.get(0).getResult();
        for (int i = 1; i < workers.size(); i++) {
            result = workers.get(i).choose(result, workers.get(i).getResult());
        }
        return result;
    }

    /**
     * Finds first max on list' elements.
     * @param <T> This describes my type parameter
     * @param i a number of threads
     * @param list a list to check
     * @param comparator shows how to compare list' elements
     * @return max
     * @throws InterruptedException if thread was interrupted
     */
    @Override
    public <T> T maximum(int i, List<? extends T> list, Comparator<? super T> comparator) throws InterruptedException {
        return choose(i, true, list, comparator);
    }

    /**
     * Finds first min on list' elements.
     * @param <T> This describes my type parameter
     * @param i a number of threads
     * @param list a list to check
     * @param comparator shows how to compare list' elements
     * @return min
     * @throws InterruptedException if thread was interrupted
     */
    @Override
    public <T> T minimum(int i, List<? extends T> list, Comparator<? super T> comparator) throws InterruptedException {
        return choose(i, false, list, comparator);
    }

    /**
     * Checks all-predicate on list' elements.
     * @param <T> This describes my type parameter
     * @param i a number of threads
     * @param list a list to check
     * @param predicate a predicate to check on
     * @return all-predicate result
     * @throws InterruptedException if thread was interrupted
     */
    @Override
    public <T> boolean all(int i, List<? extends T> list, Predicate<? super T> predicate) throws InterruptedException {
        return matches(i, true, list, predicate);
    }

    /**
     * Checks any-predicate on list' elements.
     * @param <T> This describes my type parameter
     * @param i a number of threads
     * @param list a list to check
     * @param predicate a predicate to check on
     *  @return any-predicate result
     * @throws InterruptedException if thread was interrupted
     */
    @Override
    public <T> boolean any(int i, List<? extends T> list, Predicate<? super T> predicate) throws InterruptedException {
        return matches(i, false, list, predicate);
    }
}
