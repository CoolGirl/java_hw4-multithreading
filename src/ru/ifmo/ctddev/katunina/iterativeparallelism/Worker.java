package ru.ifmo.ctddev.katunina.iterativeparallelism;

import java.util.List;

/**
 * Created by Евгения on 18.03.2015.
 */

/**
 * This class contents the description of all threads (they are Workers).
 */
public abstract class Worker<T> implements Runnable {

    /**
     * Thread begins to work on data with element with this index.
     */
    protected int startIndex;
    /**
     * Thread finishes to work on data with element with this index -1 (exclusive).
     */
    protected int finishIndex;
    /**
     * Thread works on this elements.
     */
    protected List<? extends T> data;
    /**
     * It points when the thread is finished.
     */
    protected volatile boolean finishedWork;

    /**
     * <code>finishedWork</code> getter.
     * @return  <code>finishedWork</code>
     */
    public boolean finished(){
        return finishedWork;
    }

    /**
     * Creates thread with known <code>data</code>, <code>startIndex</code> and <code>finishIndex</code>.
     * @param data a list to work on
     * @param startIndex the number of <code>data</code>' element from which we should begin
     * @param finishIndex the number of <code>data</code>' element to which we should finish (exclusive)
     * @param data the number of <code>data</code>' element to which we should finish (exclusive)
     */
    public Worker(List<? extends T> data, int startIndex, int finishIndex){
        this.data = data;

        this.startIndex = startIndex;
        this.finishIndex = finishIndex;
    }

    /**
     * Must be overrided in inheritors. Contains actions which are executed when thread is started and don't sleep.
     */
    @Override
    public abstract void run();
}
