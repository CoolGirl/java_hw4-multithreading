package ru.ifmo.ctddev.katunina.iterativeparallelism;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Евгения on 19.03.2015.
 */

/**
 * Thread which is used for maximum search.
 */
public class MaximumWorker<T> extends MinimumWorker<T> {

    /**
     * Creates a thread for maximum search.
     * @param data a list to check
     * @param startIndex the number of <code>data</code>' element from which we should begin
     * @param finishIndex the number of <code>data</code>' element to which we should finish (exclusive)
     * @param comparator shows how to compare elements
     */
    public MaximumWorker(List<? extends T> data, int startIndex, int finishIndex, Comparator<? super T> comparator) {
        super(data,startIndex, finishIndex, comparator);
    }

    /**
     * Method for two elements' comparison.
     * @param first first element to compare
     * @param second second element to compare
     * @return comparison result
     */
    @Override
    protected int compare(T first, T second) {
        return super.compare(first, second)*(-1);
    }
}
