package ru.ifmo.ctddev.katunina.iterativeparallelism;

import java.util.List;
import java.util.function.Predicate;

/**
 * Created by Евгения on 19.03.2015.
 */

/**
 * Thread which is used for any-predicate.
 */
public class AnyMatchWorker<T> extends AllMatchWorker<T> {

    /**
     * Creates a thread for searching at least one match.
     * @param data a list to check
     * @param startIndex the number of <code>data</code>' element from which we should begin
     * @param finishIndex the number of <code>data</code>' element to which we should finish (exclusive)
     * @param predicate a condition to check on
     */
    public AnyMatchWorker( List<? extends T> data, int startIndex
                          , int finishIndex, Predicate<? super T> predicate) {
        super( data,startIndex,finishIndex, predicate);
    }

    /**
     * Checks predicate on one element with the use of <code>AllMatchWorker</code> <code>check()</code> method.
     * @param value an element to try predicate on
     * @return the result of checking
     */
    @Override
    protected boolean check(T value) {
        return !super.check(value);
    }
}
