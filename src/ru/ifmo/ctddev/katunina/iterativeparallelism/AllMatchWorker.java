package ru.ifmo.ctddev.katunina.iterativeparallelism;

import java.util.List;
import java.util.function.Predicate;

/**
 * Created by Евгения on 19.03.2015.
 */

/**
 * Thread which is used for all-predicate.
 */
public class AllMatchWorker<T> extends Worker<T> {

    private Predicate<? super T> predicate;
    private volatile boolean result;

    /**
     * It is used to get the result of checking predicate on elements.
     * @return true if a predicate returned true on all elements else false
     */
    public boolean getResult() {
        return result;
    }

    /**
     * Creates a thread for checking that all elements match the condition.
     * @param data a list to check
     * @param startIndex the number of <code>data</code>' element from which we should begin
     * @param finishIndex the number of <code>data</code>' element to which we should finish (exclusive)
     * @param predicate a condition to check on
     */
    public AllMatchWorker( List<? extends T> data, int startIndex,
                          int finishIndex, Predicate<? super T> predicate) {
        super(data,startIndex,finishIndex);
        this.predicate = predicate;
    }

    /**
     * Checks all- or any-predicate on <code>data</code>' elements.
     */
    @Override
    public void run() {
        result = true;
        for (int i = startIndex; i < finishIndex && !Thread.interrupted(); i++) {
            if (!check(data.get(i))) {
                result = false;
                break;
            }
        }
        finishedWork = true;

    }

    /**
     * Checks predicate on one element.
     * @param value an element to try predicate on
     * @return the result of checking
     */
    protected boolean check(T value) {
        return predicate.test(value);
    }

}
