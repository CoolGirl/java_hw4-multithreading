package ru.ifmo.ctddev.katunina.iterativeparallelism;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Евгения on 18.03.2015.
 */

/**
 * Thread which is used for a minimum search.
 */
public class MinimumWorker<T> extends Worker<T> {

    private Comparator<? super T> comparator;
    private volatile T result;

    /**
     * It is used to get the result of searching min or max on elements.
     * @return min or max element
     */
    public T getResult() {
        return result;
    }

    /**
     * Creates a thread for minimum search.
     * @param data a list to check
     * @param startIndex the number of <code>data</code>' element from which we should begin
     * @param finishIndex the number of <code>data</code>' element to which we should finish (exclusive)
     * @param comparator shows how to compare elements
     */
    public MinimumWorker( List<? extends T> data,int startIndex, int finishIndex, Comparator<? super T> comparator) {
        super( data,startIndex,finishIndex);
        this.comparator = comparator;
    }

    /**
     * Searches minimum or maximum on all <code>data</code>' elements.
     */
    @Override
    public void run() {
        result = data.get(startIndex);
        for (int i = startIndex+1; i < finishIndex; i++) {
            result = compare(result, data.get(i)) == 1 ? data.get(i) : result;
        }

    }

    /**
     * Comparison' wrapper.
     *
     * @param first  first element to compare
     * @param second secoond element to compare
     * @return min
     */
    public T choose(T first, T second) {
        int comparisonResult;
        comparisonResult = compare(first, second);
        if (comparisonResult == 1) {
            return second;
        }
        return first; //if comparisonResult == 0 the first need to be returned (because it has less order in list)
    }

    /**
     * Compares the two elements.
     *
     * @param first  first element to compare
     * @param second secoond element to compare
     * @return comparison result
     */
    protected int compare(T first, T second) {
        int comparisonResult;
        if (comparator == null) {
            comparisonResult = ((Comparable<T>) (first)).compareTo(second);
        } else {
            comparisonResult = comparator.compare(first, second);
        }
        return comparisonResult;
    }
}
