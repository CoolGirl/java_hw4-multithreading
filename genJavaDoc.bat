mkdir tempsrc\ru
xcopy /S /Q src\ru tempsrc\ru
javadoc -linkoffline http://docs.oracle.com/javase/7/docs/api offline -d javadocs^
 -sourcepath tempsrc ru.ifmo.ctddev.katunina.iterativeparallelism info.kgeorgiy.java.advanced.concurrent
rmdir /S /Q tempsrc\ru